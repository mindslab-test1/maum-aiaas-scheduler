package maum.ai.scheduler.domain.payment;

import lombok.Getter;
import lombok.Setter;
import maum.ai.scheduler.entity.User;

@Getter
@Setter
public class UserPayDto extends User {

    private String payment;
    private String paymentDate;


    @Override
    public String toString(){
        return "userNo=" + super.getUserNo() +
                ", email='" + super.getEmail() + '\'' +
                ", name='" + super.getName() + '\'' +
                ", status=" + super.getStatus() +
                ", apiId='" + super.getApiId() +'\'' +
                ", apiKey='" + super.getApiKey() + '\'' +
                "  UserPayDto {" +
                " payment='" + payment + '\'' +
                ", paymentDate='" + paymentDate + '\'' +
                "} ";
    }

}
