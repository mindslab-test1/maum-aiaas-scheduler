package maum.ai.scheduler.domain.payment;

import lombok.Getter;
import lombok.Setter;
import maum.ai.scheduler.entity.Pay;

@Getter
@Setter
public class PayDto extends Pay {

    private String name;
    private String email;
    private String phone;
    private String company;
    private String cardName;
    private int active;
    private String paymentDate;
    private int todayPay;
    private int monthPay;
    private int allPay;
    private String currency;
    private String productName;
    private int payCount;
    private String FailReason;
    private int canCancel;

    @Override
    public String toString() {
        return  "userId=" +  super.getUserId() +
                "  PayDto{" +
                " name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", company='" + company + '\'' +
                ", cardName='" + cardName + '\'' +
                ", active=" + active +
                ", paymentDate='" + paymentDate + '\'' +
                ", todayPay=" + todayPay +
                ", monthPay=" + monthPay +
                ", allPay=" + allPay +
                ", currency='" + currency + '\'' +
                ", productName='" + productName + '\'' +
                ", payCount=" + payCount +
                ", FailReason='" + FailReason + '\'' +
                ", canCancel=" + canCancel +
                "} ";
//         + super.toString();
    }
}
