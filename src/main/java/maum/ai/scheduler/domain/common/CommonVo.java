package maum.ai.scheduler.domain.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonVo {
    private int num;
    private int createUser;
    private int updateUser;
    private String createDate;
    private String updateDate;
}
