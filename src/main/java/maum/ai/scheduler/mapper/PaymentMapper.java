package maum.ai.scheduler.mapper;

import maum.ai.scheduler.domain.payment.UserPayDto;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface PaymentMapper {

    /** 현금 결제 조회 */
    List<UserPayDto> getExpiredCashUser();
    /** 결제 내역 삭제 */
    int deleteBillInfo(int userNo);
}
