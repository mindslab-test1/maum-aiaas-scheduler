package maum.ai.scheduler.mapper;

import maum.ai.scheduler.domain.payment.UserPayDto;
import maum.ai.scheduler.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface UserMapper {
    /** UPDATE 사용자 status */
    void updateUserStatusWithUserNo(UserPayDto user);
    /** SELECT 한달 지난 INIT 사용자 */
    List<User> getExpiredInitUser();
    /** SELECT 해지 일주일 전인 INIT 사용자 */
    List<User> getExpScheduledInitUser();
}
