package maum.ai.scheduler.mapper;


import maum.ai.scheduler.entity.SupportForm;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;


@Component
@Mapper
public interface SupportMapper {

    void insertSupportMail(SupportForm supportForm);
}
