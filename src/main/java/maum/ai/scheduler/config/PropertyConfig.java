package maum.ai.scheduler.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

//@Getter
//@Setter
//@Configuration
//@EnableConfigurationProperties
//@PropertySource(value="classpath:application.yml")
public class PropertyConfig {
    private String servers;
}
