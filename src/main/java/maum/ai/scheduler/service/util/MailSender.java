package maum.ai.scheduler.service.util;

import maum.ai.scheduler.entity.SupportForm;
import maum.ai.scheduler.mapper.SupportMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MailSender {
    private final static Logger logger = LoggerFactory.getLogger(MailSender.class);


    @Autowired
    private SupportMapper supportMapper;

    /**
     * Expired date
     * DESC : 구독 해지 사용자에게 보내는 메일 (UNSUBSCRIBED 사용자)
     */

    public void sendExpiredUserMail(String emailReceiver, String name){
        String logTitle = "[ MailSender.sendExpiredUserMail(" + emailReceiver + ", " + name + ") ] ";

        logger.info(logTitle + "start");

        SupportForm supportForm = new SupportForm();

        String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
        String emailSubject = name + " 님, maum.ai 서비스 중단을 안내드립니다.";	// 제목
        String emailMsgTxt = "<!DOCTYPE html>\n" +
                "<html lang =\"ko\">\n" +
                "<head>\n" +
                "    <meta charset =\"utf-8\">\n" +
                "    <title>fail page</title>\n" +
                "</head>\n" +
                "<body style =\"margin:0; padding:100px 0; width:100%; height:100%;font-family: '나눔고딕',NanumGothic,'맑은고딕',Malgun Gothic,'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; background-color: #f7f8fc; font-size:12px; color:#666; line-height:18px;\">\n" +
                "    <div id =\"wrap\" style=\"width:650px;margin:0 auto ;  border: solid 1px #cfd5eb;\">\n" +
                "        <div id =\"header\" style =\"height:60px;position: relative;background: #fff;text-align: center;padding:0 10px; \">\n" +
                "            <h1 style =\"margin: 0;border-bottom:1px solid rgba(182, 197, 209, 0.3)\">\n" +
                "\t\t\t\t<a href=\"https://maum.ai\" target=\"_blank\" title=\"마음에이아이\">\n" +
                "\t\t\t\t\t<img src=\"https://maum.ai/aiaas/common/images/maumai.png\" alt=\"logo\" style=\"height:32px;margin:0 auto;padding:17px 0 10px;\"/>\n" +
                "\t\t\t\t</a>\n" +
                "            </h1>\n" +
                "        </div>\n" +
                "        <div id =\"container\" style =\"width:648px;margin:0 auto;\">\n" +
                "            <div id =\"content\" style =\"font-size:16px;color: #000;background: #fff;padding: 28px 24px 108px; box-sizing: border-box;text-align: center;\">\n" +
                "                <h5 style =\" font-size: 18px;font-weight: 600;letter-spacing: -0.26px;text-align: center;color: #14b6cb;margin: 0 0 20px 0;\">다시 만나길 바랍니다.</h5>\t\n" +
                "\t\t\t\t<img src =\"https://maum.ai/aiaas/common/images/mail/ico_payerror_man.png\" alt =\"computer image\" style =\"margin: 0 0 15px 0;height:100px;\"/>\n" +
                "                <p style =\"font-weight: 500;line-height: 1.71;letter-spacing: -0.4px;text-align: center;color: #34424e;font-size: 14px;margin: 0 0 22px 0;\">" + name + " 님의 마음 AI 서비스 구독이 만료되었습니다.  <br><br>\n" +				"                    아쉽지만 지금부터 모든 마음 AI의 서비스와 혜택은 제공되지 않습니다. <br>\n" +
                "                    인공지능 서비스가 필요하면 언제든 연락해주세요.\n마음 AI의 전문 컨설턴트가 문제점을 해결할 수 있도록 밀착지원 해드립니다. </p>\n" +
                "                    <strong style =\"font-size:14px; color: #407ae5;font-weight: bold;display: block;line-height: 2.5;\">바로 받는 마음 AI 혜택</strong>\n" +
                "                <ul style =\"text-align:center;list-style-position : inside; font-size: 14px;font-weight: bold;line-height: 1.79;letter-spacing: -0.4px;text-align: center;color: #407ae5;padding:0;margin:0 0 30px 0;\">\n" +
                "\t\t\t\t\t<li style =\"margin:0;\">다양한 AI 엔진 쉽게 써보기</li>\n" +
                "                    <li style =\"margin:0;\">비즈니스 사례 보기</li>\n" +
                "                    <li style =\"margin:0;\">1:1 밀착 AI 컨설팅 </li>\n" +
                "                    <li style =\"margin:0;\">Open API 사용 매뉴얼 </li>\n" +
                "                    <li style =\"margin:0;\">마음아카데미 무료 수강 </li>\n" +
                "                </ul>\n" +
                "            \n" +
                "                <a href =\"https://maum.ai/\" target =\"_blank\" style =\"display: inline-block;width:360px; height: 48px; border-radius: 2px; background-color: #14b6cb;line-height: 48px;color:#fff; font-size: 15px;letter-spacing: -0.21px;text-decoration: none\">maum.ai 바로가기</a>\n" +
                "\t\t\t\n" +
                "            </div>\n" +
                "            <div class =\"contact\" style =\"background: #fff;padding: 0 10px 20px; font-size:13px;\">\n" +
                "\t\t\t\t<div style=\"text-align: center;border-top:1px solid rgba(182, 197, 209, 0.3);color: rgba(52, 66, 78, 0.3); padding: 20px 0 0;\">\n" +
                "\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">이용약관</a>  ㅣ  \n" +
                "\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain#conditions\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">개인정보처리방침</a>  ㅣ  \n" +
                "\t\t\t\t\t<a href=\"mailto:hello@mindslab.ai\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">고객센터</a>\n" +
                "\t\t\t\t</div>\n" +
                "\t\t\t\t<p style=\"font-size: 13px;letter-spacing: -0.24px;text-align: center;color: #34424e;margin: 10px 0;\">Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</body>\n" +
                "</html>";

        // 메일보내기
        supportForm.setFromaddr(emailSender);
        supportForm.setToaddr(emailReceiver);
        supportForm.setSubject(emailSubject);
        supportForm.setMessage(emailMsgTxt);
        supportForm.setStatus(0);

        supportMapper.insertSupportMail(supportForm);
        logger.info(logTitle + "insert mail to mailQueue success");
    }

    /**
     * Expired date
     * DESC : 무료가입 후 1달 해지 일주일전 INIT 상태인 user에게 보내는 메일 (INIT 사용자)
     */

    public void sendExpScheduledInitUserMail(String emailReceiver, String name, String expDate){
        String logTitle = "[ MailSender.sendExpScheduledInitUserMail(" + emailReceiver + ", " + name + ") ] ";

        logger.info(logTitle + "start");

        SupportForm supportForm = new SupportForm();

        String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
        String emailSubject = name + " 님, 마음AI 서비스 무료체험기간이 얼마 남지 않았습니다.";	// 제목
        String emailMsgTxt = "<!DOCTYPE html>\n" +
                "<html lang =\"ko\">\n" +
                "<head>\n" +
                "    <meta charset =\"utf-8\">\n" +
                "    <title>cancel page</title>\n" +
                "</head>\n" +
                "<body style =\"margin:0; padding:100px 0; width:100%; height:100%;font-family: '나눔고딕',NanumGothic,'맑은고딕',Malgun Gothic,'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; background-color: #f7f8fc; font-size:12px; color:#666; line-height:18px;\">\n" +
                "    <div id =\"wrap\" style=\"width:650px;margin:0 auto ;  border: solid 1px #cfd5eb;\">\n" +
                "        <div id =\"header\" style =\"height:60px;position: relative;background: #fff;text-align: center;padding:0 10px; \">\n" +
                "            <h1 style =\"margin: 0;border-bottom:1px solid rgba(182, 197, 209, 0.3)\">\n" +
                "\t\t\t\t<a href=\"https://maum.ai\" target=\"_blank\" title=\"마음에이아이\">\n" +
                "\t\t\t\t\t<img src=\"https://maum.ai/aiaas/common/images/maumai.png\" alt=\"logo\" style=\"height:32px;margin:0 auto;padding:17px 0 10px;\"/>\n" +
                "\t\t\t\t</a>\n" +
                "            </h1>\n" +
                "        </div>\n" +
                "        <div id =\"container\" style =\"width:648px;margin:0 auto;\">\n" +
                "            <div id =\"content\" style =\"font-size:16px;color: #000;background: #fff;padding: 28px 24px 155px; box-sizing: border-box;text-align: center;\">\n" +
                "                <h5 style =\" font-size: 18px;font-weight: 600;letter-spacing: -0.26px;text-align: center;color: #14b6cb; margin: 0 0 28px 0;\">마음 AI 서비스를 이어가세요!</h5>\t\n" +
                "\t\t\t\t<a href=https://www.youtube.com/watch?v=Id5XLzIpCp8>\n" +
                "                    <img src=\"https://ci6.googleusercontent.com/proxy/nyMEMsRiIPYIh-ZNL8ekEOzCqdqCRc5MJ15h5APCKofJhMhtn5oXA4IB31ltfPrR9kRxDhwvDA1KG64AbBe7UJYjg8VHVww73xIa_ygOGimqOHA610hQmTg=s0-d-e1-ft#https://s3.ap-northeast-2.amazonaws.com/img.stibee.com/Id5XLzIpCp8.png\" style=\"width:100%;display:inline;vertical-align:bottom;max-width:100%;border-width:0px;border-color:initial;text-align:center\" width=\"610\" class=\"CToWUd\">\n" +
                "                </a>\n" +
                "                <p style =\"font-weight: 500;line-height: 1.71;letter-spacing: -0.4px;text-align: center;color: #34424e;font-size: 14px;margin: 34px 0 34px 0;\">"+name+" 님, 안녕하세요. <br> \n" +
                "마음 AI의 무료 기간 서비스가 " + expDate + " 종료될 예정입니다.<br>\n" +
                "                    카드 정보를 입력하여 다양한 서비스를 활용해 성공적인 비즈니스를 이어가세요. <br><br>\n" +
                "                    * 로그인 > 우측 상단 사용자 아이콘 > 결제 정보 > 카드 등록</p>\n" +
                "               \n" +
                "                <a href =\"https://maum.ai/\" target =\"_blank\" style =\"display: block;margin:0 auto;width: 360px; height: 48px; border-radius: 2px; background-color: #14b6cb;line-height: 48px;color:#fff; font-size: 15px;letter-spacing: -0.21px;text-decoration: none\">maum.ai 바로가기</a>\n" +
                "\n" +
                "            </div>\n" +
                "            <div class =\"contact\" style =\"background: #fff;padding: 0 10px 20px; font-size:13px;\">\n" +
                "\t\t\t\t<div style=\"text-align: center;border-top:1px solid rgba(182, 197, 209, 0.3);color: rgba(52, 66, 78, 0.3); padding: 20px 0 0;\">\n" +
                "\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">이용약관</a>  ㅣ  \n" +
                "\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain#conditions\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">개인정보처리방침</a>  ㅣ  \n" +
                "\t\t\t\t\t<a href=\"mailto:hello@mindslab.ai\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">고객센터</a>\n" +
                "\t\t\t\t</div>\n" +
                "\t\t\t\t<p style=\"font-size: 13px;letter-spacing: -0.24px;text-align: center;color: #34424e;margin: 10px 0;\">Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    \n" +
                "</body>\n" +
                "</html>";

        // 메일보내기
        supportForm.setFromaddr(emailSender);
        supportForm.setToaddr(emailReceiver);
        supportForm.setSubject(emailSubject);
        supportForm.setMessage(emailMsgTxt);
        supportForm.setStatus(0);

        supportMapper.insertSupportMail(supportForm);
        logger.info(logTitle + "insert mail to mailQueue success");
    }


}
