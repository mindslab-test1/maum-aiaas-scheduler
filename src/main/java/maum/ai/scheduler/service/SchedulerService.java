package maum.ai.scheduler.service;

import maum.ai.scheduler.commons.globals.EnumUserStatus;
import maum.ai.scheduler.domain.payment.UserPayDto;
import maum.ai.scheduler.entity.User;
import maum.ai.scheduler.mapper.PaymentMapper;
import maum.ai.scheduler.mapper.UserMapper;
import maum.ai.scheduler.service.apiAccount.ApiAccountService;
import maum.ai.scheduler.service.util.MailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SchedulerService {


    private static final Logger logger = LoggerFactory.getLogger(SchedulerService.class);

    private final PaymentMapper paymentMapper;
    private final UserMapper userMapper;
    private final ApiAccountService apiAccountService;
    private final MailSender mailSender;

    public SchedulerService(PaymentMapper paymentMapper, UserMapper userMapper, ApiAccountService apiAccountService, MailSender mailSender) {
        this.paymentMapper = paymentMapper;
        this.userMapper = userMapper;
        this.apiAccountService = apiAccountService;
        this.mailSender = mailSender;
    }


    @Transactional
    public void userUnsubscribedProcess(UserPayDto userPayDto){
        String logTitle = "[ SchedulerTask.userUnsubscribedProcess() ] ";
        int prevStatus = userPayDto.getStatus();

        paymentMapper.deleteBillInfo(userPayDto.getUserNo()); // 1. 사용자의 결제 정보 삭제
        logger.info(logTitle + "deleteBillingInfo success");

        userPayDto.setStatus(EnumUserStatus.UNSUBSCRIBED.getValue()); // 2. Status 값 변경
        userMapper.updateUserStatusWithUserNo(userPayDto);
        logger.info(logTitle + "update userStatus to UNSUBSCRIBED success");

        String apiId = userPayDto.getApiId();
        if(apiId == null || apiId.equals("")){
//            logger.error(logTitle + "User apiId is not exist so, can't pause API account.");
            throw new RuntimeException(logTitle + "User apiId is not exist so, can't pause API account.");
        }else{
            apiAccountService.pauseApiId(apiId); // 3. API account pause
        }

        if(prevStatus != EnumUserStatus.UNSUBSCRIBED.getValue()){
            mailSender.sendExpiredUserMail(userPayDto.getEmail(), userPayDto.getName()); // 4. 사용자에게 메일 알림
            logger.info(logTitle + " SendExpiredUserMail ==> " + userPayDto.getEmail() + " success");
        }
    }

    @Transactional
    public void initUserUnsubscribedProcess(User user){
        String logTitle = "[ SchedulerTask.initUserUnsubscribedProcess() ] ";

        UserPayDto userPayDto = new UserPayDto();
        userPayDto.setUserNo(user.getUserNo());
        userPayDto.setStatus(EnumUserStatus.UNSUBSCRIBED.getValue()); // 2. Status 값 변경
        userMapper.updateUserStatusWithUserNo(userPayDto);
        logger.info(logTitle + "update userStatus to UNSUBSCRIBED success");

        String apiId = user.getApiId();
        if(apiId == null || apiId.equals("")){
//            logger.error(logTitle + "User apiId is not exist so, can't pause API account.");
            throw new RuntimeException(logTitle + "User apiId is not exist so, can't pause API account.");
        }else{
            apiAccountService.pauseApiId(apiId); // 3. API account pause
        }

        mailSender.sendExpiredUserMail(user.getEmail(), user.getName()); // 4. 사용자에게 메일 알림
        logger.info(logTitle + " SendExpiredUserMail ==> " + user.getEmail() + " success");

    }

}
