package maum.ai.scheduler.service;

import lombok.extern.slf4j.Slf4j;
import maum.ai.scheduler.domain.payment.UserPayDto;
import maum.ai.scheduler.entity.User;
import maum.ai.scheduler.mapper.PaymentMapper;
import maum.ai.scheduler.mapper.UserMapper;
import maum.ai.scheduler.service.util.MailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class SchedulerTask {

    private static final Logger logger = LoggerFactory.getLogger(SchedulerTask.class);

    @Value("${batch.status}")
    private String mBatchStatus;

    private static final String START = "start";

    private final SchedulerService schedulerService;
    private final PaymentMapper paymentMapper;
    private final UserMapper userMapper;
    private final MailSender mailSender;

    public SchedulerTask(SchedulerService schedulerService, PaymentMapper paymentMapper, UserMapper userMapper, MailSender mailSender) {
        this.schedulerService = schedulerService;
        this.paymentMapper = paymentMapper;
        this.userMapper = userMapper;
        this.mailSender = mailSender;
    }


    /*
     * 서비스 기간이 종료된 cash 유저의 계정 만료(3시간마다)
     * [서비스 기간 기준: billing_t의 paymentDate, admin에서 설정] - 2021.01.10 MRS, YGE
     */
    @Scheduled(cron = "0 0 0/3 * * ?", zone = "Asia/Seoul")
//    @Scheduled(fixedDelay = 60000) // TODO : 시간 위에껄로 바꾸기
    public void terminateExpiredCashUsers() {

        String logTitle = "[ SchedulerTask.terminateExpiredCashUsers() ] ";

        if (START.equals(mBatchStatus)) {

            List<UserPayDto> billingCashUsers = paymentMapper.getExpiredCashUser();
            logger.info(logTitle + "start");
            logger.info(logTitle + "billingCashUser count : " + billingCashUsers.size());

            for (UserPayDto userPayDto : billingCashUsers) {
                logger.info(logTitle + "Expiring userId : " + userPayDto);
                try{
                    schedulerService.userUnsubscribedProcess(userPayDto);
                }catch (Exception e){
                    logger.error(logTitle + "userUnsubscribedProcess Exception ==> " + e);
                    e.printStackTrace();
                }
            }

        } else {
            logger.debug(logTitle + "mBatchStatus : " + mBatchStatus);
        }

    }


    /** 무료가입 후 1달 해지 일주일전 INIT 상태인 user에게 해지 예정 메일 전송 - 2021.03.10 YGE */
    @Scheduled(cron = "0 0 14 * * ?", zone = "Asia/Seoul")
//    @Scheduled(fixedDelay = 60000) // TODO : 시간 위에껄로 바꾸기
    public void sendExpScheduledInitUserMail() {

        String logTitle = "[ SchedulerTask.sendExpScheduledInitUserMail() ] ";

        if (START.equals(mBatchStatus)) {

            List<User> initUser = userMapper.getExpScheduledInitUser();
            logger.info(logTitle + "start");
            logger.info(logTitle + "expScheduledInitUser count : " + initUser.size());

            for (User user : initUser) {
                logger.info(logTitle + "userId to receive mail  : " + user.getUserNo());
                try{
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");
                    String memberCreateDate= user.getCreateDate();
                    Date date = sdf.parse(memberCreateDate);

                    cal.setTime(date);
                    cal.add(Calendar.MONTH, 1);
                    SimpleDateFormat mailSdf = new SimpleDateFormat("yyyy년 MM월 dd일");
                    String expDate = mailSdf.format(cal.getTime());
                    mailSender.sendExpScheduledInitUserMail(user.getEmail(), user.getName(), expDate);
                }catch (Exception e){
                    logger.error(logTitle + "sendExpScheduledInitUserMail Exception for userId: " + user.getUserNo() + "==> " + e);
                    e.printStackTrace();
                }
            }

        } else {
            logger.debug(logTitle + "mBatchStatus : " + mBatchStatus);
        }

    }


    /** 무료가입 후 1달이 지났지만 INIT 상태인 user를 UNSUBSCRIBED로 변경 - 2021.03.09 YGE */
    @Scheduled(cron = "0 0 12 * * ?", zone = "Asia/Seoul")
//    @Scheduled(fixedDelay = 60000) // TODO : 시간 위에껄로 바꾸기
    public void terminateInitUsers() {

        String logTitle = "[ SchedulerTask.terminateInitUsers() ] ";

        if (START.equals(mBatchStatus)) {

            List<User> initUser = userMapper.getExpiredInitUser();
            logger.info(logTitle + "start");
            logger.info(logTitle + "expiredInitUser count : " + initUser.size());

            for (User user : initUser) {
                logger.info(logTitle + "Expiring userId : " + user.getUserNo());
                try{
                    schedulerService.initUserUnsubscribedProcess(user);
                }catch (Exception e){
                    logger.error(logTitle + "initUserUnsubscribedProcess Exception for userId: " + user.getUserNo() + "==> " + e);
                    e.printStackTrace();
                }
            }

        } else {
            logger.debug(logTitle + "mBatchStatus : " + mBatchStatus);
        }

    }


}
