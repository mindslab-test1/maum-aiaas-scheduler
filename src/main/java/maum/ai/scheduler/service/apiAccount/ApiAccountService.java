package maum.ai.scheduler.service.apiAccount;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class ApiAccountService {


    /* api 서버가 불안정하여 api 2.0 적용 전까지 dev를 찌르기로 유병욱T와 협의 -- 2020-12-11 LYJ */
    @Value("${api.server.url}")
    private String apiServerUrl;
    @Value("${api.server.path.account.delete}")
    private String apiPathDelete;

    private static final Logger logger = LoggerFactory.getLogger(ApiAccountService.class);


    /* API 계정 pause -- 2021.01.10 YGE */
    public void pauseApiId(String apiId) {

        String logTitle = "[ ApiAccountService.pauseApiId("+apiId+") ] ";
        logger.info(logTitle + "start");

        Map<String, String> userApi = new HashMap<>();
        userApi.put("apiId", apiId); // apiId가 없는 경우는 SchedulerTask.TerminateExpiredCashUsers에서 처리

        String apiUrl = apiServerUrl + apiPathDelete;
        HttpURLConnection conn = null;
        try{
            URL url = new URL(apiUrl);
            conn = (HttpURLConnection) url.openConnection();
            ObjectMapper mapper = new ObjectMapper();
            String jsonValue = mapper.writeValueAsString(userApi);
            conn.setDoOutput(true);
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");

            OutputStream os = conn.getOutputStream();
            os.write(jsonValue.getBytes(StandardCharsets.UTF_8));
            os.flush();

            int apiResponseCode = conn.getResponseCode();
            logger.info(logTitle + "pauseApi responseCode : " + apiResponseCode);

            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8), 8);
            StringBuilder result = new StringBuilder();
            String inputLine;

            while ((inputLine = br.readLine()) != null) {
                try {
                    result.append((URLDecoder.decode(inputLine, StandardCharsets.UTF_8)));
                } catch (Exception ex) {
                    result.append(inputLine);
                }
            }
            br.close();

            logger.info(logTitle + "Response Result : {}", result);

            if (apiResponseCode == HttpURLConnection.HTTP_OK) { //200 성공
                logger.info(logTitle + "success");
            } else {
                logger.error(logTitle + "API request fail.");
                throw new RuntimeException(logTitle + "API request fail. responseCode : " + apiResponseCode);
            }
        }
        catch(Exception e){
            logger.error(logTitle + "exception : " + e.getMessage());
            throw new RuntimeException(logTitle + "exception : " + e.getMessage());
        }finally {
            if (conn != null) { conn.disconnect(); }
        }

    }
}
