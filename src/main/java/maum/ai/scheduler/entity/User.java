package maum.ai.scheduler.entity;

import lombok.Getter;
import lombok.Setter;
import maum.ai.scheduler.domain.common.CommonVo;

@Getter
@Setter
public class User extends CommonVo {
	private int userNo;
	private String name;
	private String id;
	private String password;
	private String email;
	private String phone;
	private int grade;
	private int product;
	private int enabled;
	private String authority;
	private int type;
	private int limit;
	private String account;
	private int voiceAgree;
	private int privacyAgree;
	private String privacyDate;
	private String lastLoginDate;
	private String lastLogoutDate;
	private int djangoid;
	private int active;
	private String Lkey;
	private String Gkey;
	private int loginFailCNT;
	private String nationCD;
	private String company;
	private String job;
	private int createFlag;
	private String companyEmail;
	private String apiKey;
	private String apiId;
	private int marketingAgree;
	private String registerPath;
	private String marketingDate;
	private int status;
	private int payCount;
	private String accessToken;
	private String accessExpireTime;
	private String refreshToken;
	private String RefreshExpireTime;
	private String channel;
}