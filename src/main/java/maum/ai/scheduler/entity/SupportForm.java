package maum.ai.scheduler.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SupportForm {

    private int id;
    private String fromaddr;
    private String toaddr;
    private String subject;
    private String message;
    private int status;
    private String senddate;
    private String name;
    private String company;
    private String mailAddr;
    private String content;
    private String phone;
    private String userEmail;

}
