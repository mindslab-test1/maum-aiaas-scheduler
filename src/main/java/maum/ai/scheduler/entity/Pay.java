package maum.ai.scheduler.entity;

import lombok.Getter;
import lombok.Setter;
import maum.ai.scheduler.domain.common.CommonVo;

@Getter
@Setter
public class Pay extends CommonVo {
    private int id;
    private int product;
    private String payment;
    private String issuer;
    private String cardNo;
    private String dateFrom;
    private String dateTo;
    private String IMP;
    private String IMPMonth;
    private int price;
    private int userId;
    private String status;
    private String tid;
    private String paNo;
    private String payDate;
    private String cancelledDate;
    private int cancelAdmin;

}
