package maum.ai.scheduler.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LogPay {
    private int no;
    private String paNo;
    private int userNo;
    private String type;
    private String payment;
    private String code;     // 카드사 코드
    private String billKey;
    private String cardNumber;
    private String createDate;
    private int status;
    private String failReason;
    private int product;
    private int price;
    private String dateFrom;
    private String dateTo;
    private String paymentDate;
}
