package maum.ai.scheduler.commons.globals;

public enum EnumUserStatus {

    INIT(0),
    FREE(1),
    SUBSCRIBE(2),
    UNSUBSCRIBING(3),
    UNSUBSCRIBED(4);

    int value;

    EnumUserStatus(int value){ this.value = value;}
    public int getValue(){ return this.value;}
}
